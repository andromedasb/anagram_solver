#!/usr/local/bin/python3.5
import itertools
import sys
from .stuff import word_set
from pprint import pprint

__version__ = "1.1.0"


def find_possible(lst):
    """
    Return all possible combinations of letters in lst

    @type lst: [str]
    @rtype: [str]
    """
    returned_list = []

    for i in range(0, len(lst) + 1):
        for subset in itertools.permutations(lst, i):
            possible = ''
            for letter in subset:
                possible += letter
            if len(possible) == len(lst):
                # itertools.permutations returns smaller lists
                returned_list.append(possible)

    return returned_list


def return_words(lst, word_set):
    """
    Return combinations in that are words in word_set

    @type lst: [str]
    @type word_set: set(str)
    @rtype: [str]
    """
    returned_list = []

    for word in lst:
        if word in word_set or word.capitalize() in word_set:
            # Some words are capitalized in the word_set
            returned_list.append(word)

    return returned_list

def find_combos(word, subset, all_words):

    if subset <= 1:
        return
    for combo in itertools.combinations(list(word), subset):
        possible_words = find_possible(''.join(combo))
        real_words = return_words(possible_words, word_set)
        all_words.extend(real_words)

        find_combos(''.join(combo), subset - 1, all_words)


def get_anagrams(word):
    """
    Main function to run the program
    """
    anagram_lst = []
    anagram = word
    for char in anagram:
        anagram_lst.append(char)

    possible_words = find_possible(anagram_lst)
    actual_words = return_words(possible_words, word_set)
    find_combos(anagram, len(anagram)-1, actual_words)

    n_letter_words = [ [] for i in range(len(anagram)-1) ]
    for word in set(actual_words):
        n_letter_words[len(word)-2].append(word)

    for group in n_letter_words:
        group.sort()

    return n_letter_words


def main():
    n_letter_words = get_anagrams(sys.argv[1])
    print('Solutions:')
    for index, n_words in enumerate(n_letter_words):
        print(f"\t {index + 2} letter words: {','.join(n_words)}")

if __name__ == '__main__':

    main()

