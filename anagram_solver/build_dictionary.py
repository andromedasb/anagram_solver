#!/usr/bin/env python
import json

dict_file = 'dictionary.json'

word_set=[]
with open(dict_file, 'r') as dict_f:
    data = json.load(dict_f)
    for word in data.keys():
        word_set.append(word.lower())


with open('stuff.py', 'w') as structured_dict:
    structured_dict.write('word_set=' + str(word_set))



